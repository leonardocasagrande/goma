// const { on } = require("gulp");




$('.menu-icon').on('click', function () {
  $('.top-nav').toggleClass('ativo');
  if($('.form-header-mobile').hasClass('aparecer')){
    $('.form-header-mobile').toggleClass('aparecer');
  }
})



$('.lock-menu').on('click', function () {
  // $('.menu-login').attr('style', 'display:block');
  $('.menu-login').addClass('opacity-event');
})
$('.lock-menu-mobile').on('click', function () {
  // $('.menu-login').attr('style', 'display:block');
  $('.form-header-mobile').toggleClass('aparecer');
})
$('.close-login').on('click', function () {
  // $('.menu-login').attr('style', 'display:none');
  $('.menu-login').removeClass('opacity-event');
})




function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$('.e-mail').attr('onChange', "validateEmail(this)")


$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

var sliderServicos = tns({
  container: '.carousel-servicos',
  items: 1,
  slideBy: 1,
  autoplay: false,
  loop: false,
  touch: false,
  autoplayButtonOutput: false,
  controls: true,
  nav: false,
  controlsPosition: 'bottom',
  controlsContainer: '.controls-servicos',
});

const infoServico = sliderServicos.getInfo();
let serviceIndex = infoServico.displayIndex;

$('.controls-servicos .right').on('click', function () {
  if (serviceIndex < infoServico.slideCount) {
    serviceIndex++;
  }
})

$('.controls-servicos .left').on('click', function () {
  if (serviceIndex > 1) {
    serviceIndex--;
  }
})

$('.controls-servicos').on('click', function () {
  console.log(serviceIndex)

  if (serviceIndex === 1) {
    $('.controls-servicos .left').addClass('hidden');
  } else {
    $('.controls-servicos .left').removeClass('hidden');

  }
  if (serviceIndex === infoServico.slideCount) {
    $('.controls-servicos .right').addClass('hidden');
  } else {
    $('.controls-servicos .right').removeClass('hidden');

  }
})


var sliderPortfolio = tns({
  container: '.carousel-portfolio',
  items: 1,
  slideBy: 1,
  autoplay: false,
  loop: false,
  touch: false,
  autoplayButtonOutput: false,
  controls: true,
  nav: false,
  controlsPosition: 'bottom',
  controlsContainer: '.controls-portfolio',
  responsive: {
    700: {
      items: 2,
      gutter: 30,
    },
  }
});


const infoPortfolio = sliderPortfolio.getInfo();

let portfolioIndex = infoPortfolio.displayIndex;

$('.controls-portfolio .right').on('click', function () {
  if (portfolioIndex < infoPortfolio.slideCount) {
    portfolioIndex++;
  }
})

$('.controls-portfolio .left').on('click', function () {
  if (portfolioIndex > 1) {
    portfolioIndex--;
  }
})

$('.controls-portfolio').on('click', function () {
  console.log(portfolioIndex)

  if (portfolioIndex === 1) {
    $('.controls-portfolio .left').addClass('hidden');
  } else {
    $('.controls-portfolio .left').removeClass('hidden');

  }
  if (portfolioIndex === infoPortfolio.slideCount) {
    $('.controls-portfolio .right').addClass('hidden');
  } else {
    $('.controls-portfolio .right').removeClass('hidden');

  }
})


var sliderPortfolio2 = tns({
  container: '.carousel-portfolio-2',
  items: 1,
  slideBy: 1,
  autoplay: false,
  loop: false,
  touch: false,
  autoplayButtonOutput: false,
  controls: true,
  nav: false,
  controlsPosition: 'bottom',
  controlsContainer: '.controls-portfolio-2',
});

const infoPortfolioLg = sliderPortfolio2.getInfo();

let portfolioIndexLg = infoPortfolioLg.displayIndex;

$('.controls-portfolio-2 .right').on('click', function () {
  if (portfolioIndexLg < infoPortfolioLg.slideCount) {
    portfolioIndexLg++;
  }
})

$('.controls-portfolio-2 .left').on('click', function () {
  if (portfolioIndexLg > 1) {
    portfolioIndexLg--;
  }
})

$('.controls-portfolio-2').on('click', function () {
  console.log(portfolioIndexLg)

  if (portfolioIndexLg === 1) {
    $('.controls-portfolio-2 .left').addClass('hidden');
  } else {
    $('.controls-portfolio-2 .left').removeClass('hidden');

  }
  if (portfolioIndexLg === infoPortfolioLg.slideCount) {
    $('.controls-portfolio-2 .right').addClass('hidden');
  } else {
    $('.controls-portfolio-2 .right').removeClass('hidden');

  }
})


let responseForm = $('.wpcf7-response-output')[0].attributes[1].value;

$('.wpcf7-submit').on('click', function () {
  console.log(responseForm)

  setTimeout(function () {
    if (responseForm) {
      $('.yellow-lg ').attr('style', 'height:480px ');
    } else {
      $('.yellow-lg ').attr('style', 'height:430px ');
    }
  }, 300)
})



