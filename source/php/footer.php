<?php $usuario = wp_get_current_user();?>
<?php if (is_page('home')) : ?>
  <footer id="contato" class="footer-home">

    <div class="footer-lg">

      <div class="container  col-lg-6 pr-lg-0">

        <div class="box container pr-lg-0">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png" alt="">

          <div class="infos col-8 px-0">

            <a href="https://www.google.com.br/maps/dir//Av.+Ivo+Trevisan,+206+-+Jardim+Joao+Paulo+II,+Sumar%C3%A9+-+SP,+13172-705/@-22.8237343,-47.296641,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x94c8a2adbccaffeb:0x7d8fc38e376dbef!2m2!1d-47.2944523!2d-22.8237343" target="_blank" class="info">
              <i class="fas fa-map-marker-alt"></i>
              <span>Av. Ivo Trevisan, 206 Jardim Consteca, Sumaré - SP, 13172-640</span>
            </a>

            <a href="mailto:obras@gomaengenharia.com.br" class="info">
            <i class="far fa-envelope"></i>
            <span>obras@gomaengenharia.com.br</span>
            </a>
            
            <!-- <a href="tel:+5519999771077" target="_blank" class="info">
              <i class="fas fa-phone-alt"></i>
              <span>(19) 99977-1077</span>
            </a> -->

            <a href="https://api.whatsapp.com/send?phone=5519999771077" target="_blank" class="info">
              <i class="fab fa-whatsapp"></i>
              <span>(19) 99977-1077</span>
            </a>
          </div>

        </div>

      </div>

      <div class="yellow-lg d-none d-lg-block"></div>

      <div class="form-box col-lg-6 px-lg-0">

        <div class="container px-lg-0">

          <span class="title">ENTRE EM CONTATO</span>


          <?= do_shortcode('[contact-form-7 id="23" title="Formulário de contato 1"]'); ?>

          <!-- <input type="text" name="nome" placeholder="Nome" id="">
          <input type="email" name="email" placeholder="E-mail" id="">
          <textarea name="" placeholder="Mensagem" id="" cols="30" rows="10"></textarea>
          <input type="submit" value="enviar" class="btn-cta"> -->

        </div>

      </div>

      <div class="container d-lg-none">
        <div class="footer-detail"></div>
      </div>

    </div>

    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/footer-lg.png" alt="" class="d-none d-lg-block footer-detail-lg">

  </footer>


<?php else : ?>

  <footer class="footer-login">

    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/profile.png" alt="">

    <span class="hello-name">Olá, <span class="name"><?= $usuario->display_name ?></span></span>

  </footer>



<?php endif; ?>









<?php wp_footer(); ?>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/lightbox.min.js"></script>


<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>
<script src="//tag.goadopt.io/injector.js?website_code=a264db25-e5a9-4fdb-b150-38d3364b5830" class="adopt-injector"></script>
<script>
  AOS.init();
</script>

</body>

</html>