<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PSPS7B5');</script>
  <!-- End Google Tag Manager -->

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title> Goma Engenharia</title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/> -->
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/lightbox.min.css">


  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PSPS7B5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php $usuario = wp_get_current_user();?>
  <?php if (is_page('home')) : ?>

    <div class="menu-login">
      <div class="container">
        <div class="header-login">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="" class="logo">

          <a href="#" class="close-login"><i class="fas fa-times"></i></a>
        </div>

        <div class="content">

          <span class="title">área <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado-lg.png" alt=""></br>restrita</span>

          <?php 
            global $user_login;

            // If user is already logged in.
            if ( is_user_logged_in() ) : ?>

                <div class="aa_logout color-white"> 
                    Olá,
                    <?php 
                       
                        
                        echo $usuario->display_name;
                        
                    ?>
                    
                    </br>
                    <a href="<?= home_url( '/home-user/' ) ?>" class="color-white">Clique aqui para acessar a área restrita</a>
                    

                </div>

                <a id="wp-submit" class="color-white" href="<?php echo wp_logout_url(get_bloginfo('url')); ?>" title="Logout">
                    <?php _e( 'Clique aqui para sair', 'AA' ); ?>
                </a>

            <?php 
                // If user is not logged in.
                else: 
                 echo '<div class="form-box col-lg-6 px-0">';
                    // Login form arguments.
                    $args = array(
                        'echo'           => true,
                        'redirect'       => home_url( '/home-user/' ), 
                        'form_id'        => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Entrar' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => true,
                        'value_username' => NULL,
                        'value_remember' => true
                    ); 
                    
                    // Calling the login form.
                    wp_login_form( $args );
                  echo '</div>

                  <div class="yellow-square"></div>';
                endif;
        ?> 

          

            <!-- <input type="text" name="" placeholder="Login" id="">

            <input type="password" name="" placeholder="Senha" id="">

            <input type="submit" value="entrar" class="btn-cta"> -->

          

        </div>

      </div>
    </div>




    <header class="d-lg-block d-none  ">


        <div class="header ">
      <div class="container">

          <a href="<?= get_site_url(); ?>/">
            <img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png" alt="">
          </a>

          <div class="links  px-0">

            <a href="#sobre-nos">Sobre nós</a>
            <a href="#nossos-servicos">Nossos serviços</a>
            <a href="#nosso-diferencial">Nosso diferencial</a>
            <a href="#portfolio">Portfólio</a>
            <a href="#contato">Contato</a>
            <a class="lock-menu" href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado.png" alt=""></a>

          </div>


        </div>
      </div>



    </header>


    <nav class=" d-lg-none top-nav  " id="top-nav">

      <a href="<?= get_site_url(); ?>">

        <img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png" alt="goma">

        <img class="logo-header-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="goma">
      </a>

      <input class="menu-btn" type="checkbox" id="menu-btn" />

      <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

      <div class="menu">

        <a href="#sobre-nos">Sobre nós</a>
        <a href="#nossos-servicos">Nossos serviços</a>
        <a href="#nosso-diferencial">Nosso diferencial</a>
        <a href="#portfolio">Portfólio</a>
        <a href="#contato">Contato</a>


        <div class="midias-header">
          <a href="#" class="lock-menu-mobile"> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado-h-user.png" alt=""></a>
          <a href="https://www.instagram.com/gomaengenharia/" target="_blank"><i class="fab fa-instagram"></i></a>
          <a href="https://www.facebook.com/gomaengenharia" target="_blank"><i class="fab fa-facebook-f"></i></a>
        </div>

      </div>

      <div class="form-header form-header-mobile">
      <a class="lock-menu-mobile position-absolute form-lock" href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/close-form.png" alt=""></a>

      <?php // If user is already logged in.
            if ( is_user_logged_in() ) : ?>

                <div class="aa_logout color-white"> 
                    Olá,
                    <?php 
                       
                        
                        echo $usuario->display_name;
                        
                    ?>
                    
                    </br>
                    <a href="<?= home_url( '/home-user/' ) ?>" class="color-white">Clique aqui para acessar a área restrita</a>
                    

                </div>

                <a id="wp-submit" class="color-white" href="<?php echo wp_logout_url(get_bloginfo('url')); ?>" title="Logout">
                    <?php _e( 'Clique aqui para sair', 'AA' ); ?>
                </a>

            <?php 
                // If user is not logged in.
                else: 
                 echo '<div class="form-box col-lg-6 px-0">';
                    // Login form arguments.
                    $args = array(
                        'echo'           => true,
                        'redirect'       => home_url( '/home-user/' ), 
                        'form_id'        => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Entrar' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => true,
                        'value_username' => NULL,
                        'value_remember' => true
                    ); 
                    
                    // Calling the login form.
                    wp_login_form( $args );
                  echo '</div>

                  <div class="yellow-square"></div>';
                endif;
        ?> 

        <!-- <input class="col-12 " type="text" name="login" id="" placeholder="Login">

        <input class="col-12 " type="password" name="pass" id="" placeholder="Senha">

        <input type="submit" value="entrar" class="btn-cta"> -->

      </div>

    </nav>



  <?php else : ?>


    <header class="d-lg-block d-none">
      <div class="menu-box ">

        <div class="container">

          <a href="<?= get_site_url(); ?>/">
            <img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header-user.png" alt="">
          </a>

          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link menu-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Meu projeto</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link menu-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Meu perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link menu-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Ajuda</a>
            </li> -->
          </ul>


        </div>

      </div>
    </header>

    <nav class=" d-lg-none  custom-nav  ">

      <div class="top-nav " id="top-nav">

        <a href="<?= get_site_url(); ?>"><img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="goma"></a>
       <?php if ( is_user_logged_in() ) : ?>
        <a href="<?php echo  wp_logout_url(get_bloginfo('url')); ?>" class="logout-btn">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logout-icon.png" alt="">
        </a>
        <?php endif; ?>
      </div>

      <h1 class="title-banner">Àrea restrita</h1>

    </nav>




  <?php endif; ?>