<?= get_header();  $usuario = wp_get_current_user(); $user_id = 'user_'.$usuario->ID?>

<div class="menu-box d-lg-none ">

  <div class="container">

    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link menu-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Meu projeto</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link menu-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Meu perfil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link menu-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Ajuda</a>
      </li> -->
    </ul>


  </div>

</div>

<section class="lg-custom">


  <div class="header-lg d-none d-lg-block">

    <div class="container">
      <span class="title">área restrita <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado-h-user.png" alt=""></span>

      <a href="<?= wp_logout_url(get_bloginfo('url')); ?>" class="btn-logout">logout <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logout.png" alt=""></a>
    </div>

  </div>

  <div class="tab-content section-slide container" id="pills-tabContent">



    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

      <div class="header-section projeto">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/projeto-icon.png" alt="">

        <div>

          <span class="title">Projeto: <span class="project-title"><?php the_field('nome_do_projeto', $user_id) ?></span></span>

          <?php the_field('informacoes_do_projeto', $user_id) ?>
        </div>

      </div>

      <div class="arquivos"> <!-- inicio dos arquivos -->

        <div class="d-flex align-items-center header-section">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/file-icon.png" alt="">

          <span class="title">Relatórios financeiros</span>

        </div>

        <?php if( have_rows('arquivos_financeiros', $user_id) ): ?>
        <ol class="list-arquivos">
        <?php while( have_rows('arquivos_financeiros', $user_id) ): the_row(); ?>
        <?php $arquivo = get_sub_field('arquivo');  ?>
          <li>
          <a href="<?= $arquivo['url']; ?>" target="_blank" class="arquivo  px-0">
            
            <span class="file-name"><?= $arquivo['title']; ?></span>
          </a>
          </li>
        <?php endwhile; ?>
          

          

        </ol>
        <?php endif; ?>
      </div> <!-- fim dos arquivos -->

      <div class="arquivos"> <!-- inicio dos arquivos -->

        <div class="d-flex align-items-center header-section">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/file-icon.png" alt="">

          <span class="title">Diário de obra</span>

        </div>

        <?php if( have_rows('arquivos_obra', $user_id) ): ?>
        <ol class="list-arquivos">
        <?php while( have_rows('arquivos_obra', $user_id) ): the_row(); ?>
        <?php $arquivo = get_sub_field('arquivo');  ?>
          <li>
          <a href="<?= $arquivo['url']; ?>" target="_blank" class="arquivo  px-0">
            
            <span class="file-name"><?= $arquivo['title']; ?></span>
          </a>
          </li>
        <?php endwhile; ?>
          

          

        </ol>
        <?php endif; ?>
      </div> <!-- fim dos arquivos -->

      <div class="arquivos"> <!-- inicio dos arquivos -->

        <div class="d-flex align-items-center header-section">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/file-icon.png" alt="">

          <span class="title">Cronograma</span>

        </div>

        <?php if( have_rows('arquivos_cronogramas', $user_id) ): ?>
        <ol class="list-arquivos">
        <?php while( have_rows('arquivos_cronogramas', $user_id) ): the_row(); ?>
        <?php $arquivo = get_sub_field('arquivo');  ?>
          <li>
          <a href="<?= $arquivo['url']; ?>" target="_blank" class="arquivo  px-0">
            
            <span class="file-name"><?= $arquivo['title']; ?></span>
          </a>
          </li>
        <?php endwhile; ?>
          

          

        </ol>
        <?php endif; ?>
      </div> <!-- fim dos arquivos -->

      <div class="arquivos"> <!-- inicio dos arquivos -->

        <div class="d-flex align-items-center header-section">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/file-icon.png" alt="">

          <span class="title">Fotos</span>

        </div>

        <?php if( have_rows('arquivos_fotos', $user_id) ): ?>
        <div class="arquivo-box">
        <?php while( have_rows('arquivos_fotos', $user_id) ): the_row(); ?>
        <?php $arquivo = get_sub_field('arquivo');  ?>
          <a href="<?= $arquivo['url']; ?>" target="_blank" class="arquivo  px-0">
            <div class="box-logo">
              <img src="<?= $arquivo['url']; ?>" alt="" class="img-fluid fotinha">
              <!-- <div class="color-logo <?php if($arquivo['subtype'] == 'pdf'){echo "blue";}else{echo "yellow";} ?>">
                <span class="extension"><?= $arquivo['subtype']; ?></span>
              </div> -->
            </div>
            <span class="file-name"><?= $arquivo['title']; ?></span>
          </a>
        <?php endwhile; ?>
          

          

        </div>
        <?php endif; ?>
      </div> <!-- fim dos arquivos -->

      <div class="arquivos"> <!-- inicio dos arquivos -->

        <div class="d-flex align-items-center header-section">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/file-icon.png" alt="">

          <span class="title">Lembretes</span>

        </div>

        <?php if( have_rows('arquivos_lembretes', $user_id) ): ?>
        <ol class="list-arquivos">
        <?php while( have_rows('arquivos_lembretes', $user_id) ): the_row(); ?>
        <?php $arquivo = get_sub_field('arquivo');  ?>
          <li>
          <a href="<?= $arquivo['url']; ?>" target="_blank" class="arquivo  px-0">
            
            <span class="file-name"><?= $arquivo['title']; ?></span>
          </a>
          </li>
        <?php endwhile; ?>
          

          

        </ol>
        <?php endif; ?>
      </div> <!-- fim dos arquivos -->

      <div class="infos">

        <div class="d-flex align-items-center header-section">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/resp-icon.png" alt="">

          <div>
            <span class="title">Responsável:</span>
            <span class="project-title"><?php the_field('nome_do_responsavel', $user_id) ?> </span>
          </div>

        </div>

        <div class="contact col-lg-6">
          <div class="d-lg-flex">
            <a href="tel:+55<?php the_field('telefone', $user_id) ?>" class="col-6 pl-0"><i class="fas fa-phone-alt"></i>
            <?php
                $data= get_field('telefone',$user_id);
               echo formatPhone($data);
            ?>
          </a>
            <a href="https://api.whatsapp.com/send?phone=55<?php the_field('telefone', $user_id) ?>" class="col-6 pl-0"><i class="fab fa-whatsapp"></i><?= formatPhone($data); ?></a>
          </div>

          <a href="mail:<?php the_field('email', $user_id) ?>" class="col-12 px-0"><i class="fas fa-envelope"></i><?php the_field('email', $user_id) ?></a>
        </div>


      </div>


      <img class="detail-bottom d-md-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/footer-login-detail.png" alt="">

      <img class="detail-bottom d-none d-md-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/footer-login-detail2.png" alt="">



    </div>



    <!-- <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">FALTA CRIATIVO FAZER</div>



    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">FALTA CRIATIVO FAZER</div> -->




  </div>

</section>

<?= get_footer(); ?>